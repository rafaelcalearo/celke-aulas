<?php

namespace App\Controller;

use App\Controller\AppController;

class UsersController extends AppController
{
	public  function index() 
	{
		//$usuario = "Rafael";
		//$this->set(['usuario' => $usuario]);

		//$usuarios = $this->Users->find()->all();
		//$this->set(['usuarios' => $usuarios]);

		$this->paginate = [
			'limit' => 5,
			'order' => [
				'Users.id' => 'asc'
			]
		];

		//$usuarios = $this->Users->find()->all();

		$usuarios = $this->paginate($this->Users);
		$this->set(compact('usuarios'));
	}

	//O null E ATRIBUIDO P/ $id CASO NAO SEJA PASSADO NENHUM REGISTRO PELA VIEW, ASSIM NAO DA ERRO!
	public function view($id = null)
	{
		$usuario = $this->Users->get($id);

		$this->set(['usuario' => $usuario]);		
	}

	public function add()
	{
		$user = $this->Users->newEntity();

		if($this->request->is('post')){
			$user = $this->Users->patchEntity($user, $this->request->getData());
			//debug($user);
			//exit;
			if($this->Users->save($user)){
				$this->Flash->success(__('Usuário cadastrado com sucesso'));
				return $this->redirect(['action' => 'index']);
			} else{
				//$this->Flash->success(__('Erro: Usuário não foi cadastrado com sucesso'));
				//MODIFIQUEI P/ O METODO error()
				$this->Flash->error(__('Erro: Usuário não foi cadastrado com sucesso'));
			}
		}

		$this->set(compact('user'));
	}

	public function edit($id = null)
	{
		$usuario = $this->Users->get($id);

		if($this->request->is(['post', 'put'])){
			$usuario = $this->Users->patchEntity($usuario, $this->request->getData());
			//debug($user);
			//exit;
			if($this->Users->save($usuario)){
				$this->Flash->success('Usuário editado com sucesso');
				return $this->redirect(['action' => 'index']);
			} else{
				$this->Flash->error('Usuário não foi editado com sucesso');
			}
		}

		$this->set(compact('usuario'));
	}

	public function delete($id = null)
	{
		$this->request->allowMethod(['post', 'delete']);
		$usuario = $this->Users->get($id);
		if($this->Users->delete($usuario)){
			$this->Flash->success('Usuário apagado com sucesso');
		} else{
			$this->Flash->error('O usuário não foi apagado com sucesso');
		}
		return $this->redirect(['action' => 'index']);
	}
}